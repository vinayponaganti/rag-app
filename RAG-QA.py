import streamlit as st
from langchain_community.document_loaders import UnstructuredExcelLoader
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain_community.vectorstores.utils import filter_complex_metadata
from langchain_google_genai import GoogleGenerativeAIEmbeddings, ChatGoogleGenerativeAI
from langchain_core.prompts import ChatPromptTemplate
from langchain.chains.combine_documents import create_stuff_documents_chain
from langchain.chains import create_retrieval_chain
from langchain_community.vectorstores import Chroma
import warnings
warnings.filterwarnings("ignore")
import streamlit as st
from PyPDF2 import PdfReader
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain_google_genai import GoogleGenerativeAIEmbeddings
from langchain_community.embeddings import HuggingFaceEmbeddings
from langchain_google_genai import ChatGoogleGenerativeAI
from langchain.chains import RetrievalQA
from langchain.chains import ConversationChain
from langchain.chains.question_answering import load_qa_chain
from langchain.prompts import PromptTemplate
from langchain_community.vectorstores import Chroma
from langchain_community.document_loaders import UnstructuredFileLoader, UnstructuredExcelLoader, PyPDFLoader, UnstructuredPowerPointLoader, CSVLoader, Docx2txtLoader
from bs4 import BeautifulSoup
from langchain_google_genai import GoogleGenerativeAI
import streamlit.components.v1 as stc
import google.generativeai as genai
from pathlib import Path
import mimetypes
from pptx import Presentation
import json
from pathlib import Path
import logging
import pandas as pd
import os
import tempfile
import unstructured
from PIL import Image
from pathlib import Path
import mimetypes
from langchain.memory import ConversationBufferMemory
from langchain_community.document_loaders import YoutubeLoader
from langchain_community.document_loaders.sitemap import SitemapLoader
# Set up Google API key
api_key = "AIzaSyBD_g1FZAOsyPRQvBGwoGM3uxNB3pq38Es"
genai.configure(api_key=api_key)
embeddings=HuggingFaceEmbeddings(model_name="sentence-transformers/all-MiniLM-L6-v2")
#vector_db1=None
from langchain.chat_models import ChatOpenAI
from langchain.chains import LLMChain
from langchain.prompts import PromptTemplate
from decouple import config
from langchain.memory import ConversationBufferWindowMemory
from langchain.chains import ConversationalRetrievalChain
import streamlit.components.v1 as components
import time
import urllib.parse

from langchain.prompts import (
    SystemMessagePromptTemplate,
    HumanMessagePromptTemplate,
    ChatPromptTemplate,
    MessagesPlaceholder
)
from streamlit_chat import message
# Set up logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


##function to load data from the images 

# Model Configuration
MODEL_CONFIG = {
  "temperature": 0.0,
  "top_p": 1,
  "top_k": 32,
  "max_output_tokens": 4096,
}

## Safety Settings of Model
safety_settings = [
  {
    "category": "HARM_CATEGORY_HARASSMENT",
    "threshold": "BLOCK_MEDIUM_AND_ABOVE"
  },
  {
    "category": "HARM_CATEGORY_HATE_SPEECH",
    "threshold": "BLOCK_MEDIUM_AND_ABOVE"
  },
  {
    "category": "HARM_CATEGORY_SEXUALLY_EXPLICIT",
    "threshold": "BLOCK_MEDIUM_AND_ABOVE"
  },
  {
    "category": "HARM_CATEGORY_DANGEROUS_CONTENT",
    "threshold": "BLOCK_MEDIUM_AND_ABOVE"
  }
]

# Initialize GenAI GenerativeModel
model = genai.GenerativeModel(model_name="gemini-pro-vision", generation_config=MODEL_CONFIG, safety_settings=safety_settings)

# Valid image formats
VALID_FORMATS = ['image/jpeg', 'image/png', 'image/gif']

# Function to process image and extract text
def upload_image(image_file):
    #logger.info("Uploading image")
    # Save the uploaded image to a temporary location
    temp_image_path = os.path.join(tempfile.gettempdir(), image_file.name)
    with open(temp_image_path, "wb") as f:
        f.write(image_file.read())

    # Process the uploaded image to extract text
    text_from_image = process_image_and_answer(temp_image_path)

    # Delete the temporary image file
    os.remove(temp_image_path)

    return text_from_image

# Function to process image and generate text
def process_image_and_answer(image_path):
    def image_format(image_path):
        img = Path(image_path)
        if not img.exists():
            raise FileNotFoundError(f"Image file not found: {img}")

        mime_type, _ = mimetypes.guess_type(str(img))
        if not mime_type or mime_type not in VALID_FORMATS:
            raise ValueError(f"Invalid image format: {img}")

        image_parts = [
            {
                'mime_type': mime_type,
                'data': img.read_bytes()
            }
        ]
        return image_parts

    def gemini_output(image_path):
        image_info = image_format(image_path)
        response = model.generate_content(image_info)
        return response.text
    logger.info(f"Processing image: {image_path}")
    output = gemini_output(image_path)
    return output

### function to load text from youtube video

def load_websites(url):
    #logger.info(f"Loading website: {url}")
    path = url + "sitemap.xml"
    sitemap_loader = SitemapLoader(web_path=path, continue_on_failure=True)
    documents = sitemap_loader.load()
    return documents

def extract_text(url):
    logger.info(f"Extracting text from URL: {url}")
    try:
        docs = load_websites(url)
        if docs:
            return docs
    except Exception as e:
        pass  # Ignore errors from website loader
    
    if "youtube.com" in url:
        try:
            docs = YoutubeLoader.from_youtube_url(url, add_video_info=True).load()
            return docs
        except ValueError as e:
            return f"Error processing YouTube URL '{url}': {e}"

    return f"Unsupported URL '{url}'"



# Function to load data from file

@st.cache(allow_output_mutation=True)
def load_data_from_file(file):
    file_extension = Path(file.name).suffix.lower()
    temp_file_path = None
    try:
        with tempfile.NamedTemporaryFile(delete=False) as temp_file:
            temp_file.write(file.read())
            temp_file_path = temp_file.name
            temp_file.close()

            if file_extension == ".pdf":
                loader = PyPDFLoader(temp_file_path)
            elif file_extension == ".pptx":
                loader = UnstructuredPowerPointLoader(temp_file_path)
            elif file_extension == ".csv":
                loader = CSVLoader(temp_file_path)
            elif file_extension == ".docx":
                loader = Docx2txtLoader(temp_file_path)
            elif file_extension == ".txt":
                with open(temp_file_path, 'r') as txt_file:
                    return txt_file.read()  # Read the text content directly
                #loader = UnstructuredFileLoader(temp_file_path)
            elif file_extension == ".xlsx":
                loader = UnstructuredExcelLoader(temp_file_path, mode="elements")
                xlsx_data = loader.load()
                html_content = str(xlsx_data)
                soup = BeautifulSoup(html_content, 'html.parser')
                rows = soup.find('tbody').find_all('tr')
                excel_data = []
                for row in rows:
                    cells = row.find_all('td')
                    excel_data.extend([cell.get_text() for cell in cells])
                return excel_data
            elif file_extension == ".json":
                with open(temp_file_path, 'r') as json_file:
                    return json.load(json_file)  # Load JSON data from file
                    
                
            else:
                raise ValueError(f"Unsupported file type: {file_extension}")

            return loader.load()
    finally:
        if temp_file_path and os.path.exists(temp_file_path):
            os.remove(temp_file_path)


@st.cache(allow_output_mutation=True)
def get_text_chunks(text):
    text_splitter = RecursiveCharacterTextSplitter(chunk_size=10000, chunk_overlap=1000)
    chunks = text_splitter.split_text(text)
    return chunks

@st.cache(allow_output_mutation=True)
def get_vector_store1(text_chunks, path, file_name):
    # Create a directory with the file name if it doesn't exist
    file_directory = os.path.join(path,file_name)
    print("the directory is",file_directory) 
    if not os.path.exists(file_directory):  
        os.makedirs(file_directory)
    
    # Persist embeddings in the directory with the file name
    print("the directory is",file_directory)
    persist_directory = file_directory
    vectordb = Chroma.from_texts(text_chunks, embeddings, persist_directory=persist_directory)
    return vectordb

def get_conversational_chain(loc):
    template = """
     Act as a senior search assistant. Use the context provided below to answer questions. If the question is not related to the context, respond with "This question is out of context. Please ask something related to the provided context."
     Use the following context (delimited by <ctx></ctx>) and the chat history (delimited by <hs></hs>) to answer the question:
            ------
            <ctx>
            {context}
            </ctx>
            ------
            <hs>
            {chat_history}
            </hs>
            ------
            {question}
            Answer: 
"""
    prompt = ChatPromptTemplate.from_template(template)
    memory = ConversationBufferMemory(memory_key="chat_history", return_messages=True)
    
    llm = GoogleGenerativeAI(model="models/text-bison-001", google_api_key=api_key, temperature=0, verbose=True)
 
    new_db = Chroma(persist_directory=loc, embedding_function=embeddings)
    retriever = new_db.as_retriever()
    
    qa = ConversationalRetrievalChain.from_llm(
        llm=llm,
        retriever=retriever, 
        memory=memory,
        verbose=False
    )
    
    return qa

def main():
    # Set Streamlit page configuration
    st.set_page_config(
        page_title="Streamlit App",
        page_icon="🤖",
        layout="wide"
    )
    # Set theme to dark
    st.markdown(
        """
        <style>
        body {
            color: white;
            background-color: #1E1E1E;
        }
        </style>
        """,
        unsafe_allow_html=True
    )
    # Custom HTML for the title
    st.markdown(
    """
    <h1 style='text-align: center; color: #4287f5; font-family: Arial, sans-serif; font-weight: bold;'>
        Chat with Any Type of Files/Images/URL's:
    </h1>
    """,
    unsafe_allow_html=True
    )
    
    menu = ["Images", "Files", "URLs"]
    choice = st.sidebar.selectbox("Menu", menu)

    if choice == "Images":
        handle_images()
    elif choice == "Files":
        handle_files()
    elif choice == "URLs":
        handle_urls()

def handle_images():
    st.sidebar.subheader("Upload Images")
    uploaded_image = st.sidebar.file_uploader("Upload Image", type=["jpg", "jpeg", "png"])
    if uploaded_image is not None:
        path = "db_DB"
        loc = os.path.join(path, uploaded_image.name)

        if not os.path.exists(loc):
            st.write("We got a new file. Please wait, we are processing it for you:")
            text = upload_image(uploaded_image)
            chunks = get_text_chunks(str(text))
            vectors = get_vector_store1(chunks, path, uploaded_image.name)
        else:
            # File already exists, use a flag to avoid reprocessing
            if 'image_processed' not in st.session_state:
                st.write("We got a new file. Please wait, we are processing it for you:")
                text = upload_image(uploaded_image)
                chunks = get_text_chunks(str(text))
                vectors = get_vector_store1(chunks, path, uploaded_image.name)
                st.session_state['image_processed'] = True
            else:
                st.write("File already exists. Using cached data.")

        pass_loc(loc)


def handle_files():
    st.sidebar.subheader("Upload Files")
    file = st.sidebar.file_uploader("Upload a file")
    if file is not None:
        path = "db_DB"
        loc = os.path.join(path, file.name)
        if not os.path.exists(loc):
            st.write("We got a new file. Please wait, we are processing it for you:")
            if st.button("Submit & Process"):
                raw_text = load_data_from_file(file)
                chunks = get_text_chunks(str(raw_text))
                vectors = get_vector_store1(chunks, path, file.name)
                st.text("Extracted Text:")
                st.write(raw_text)
                st.write(vectors)
                pass_loc(loc)
        else:
            st.write("File already exists. Please wait, we are loading it for you:")
            pass_loc(loc)

def handle_urls():
    url = st.text_input("Enter URL")
    if url:
        path = "db_DB"
        if "youtube" in url:
            loc = os.path.join(path, url[-8:-4])
            url1 = url[-8:-4]
        else:
            loc = os.path.join(path, url[8:])
            url1 = url[8:]
        if not os.path.exists(loc):
            st.write("We got a new file. Please wait, we are processing it for you:")
            url_text = extract_text(url)
            chunks = get_text_chunks(str(url_text))
            vectors = get_vector_store1(chunks, path, url1)
            st.write(url_text)
            pass_loc(loc)
        else:
            st.write("File already exists. Please wait, we are loading it for you:")
            pass_loc(loc)

def pass_loc(loc):
    if 'chat_history' not in st.session_state:
        st.session_state.chat_history = []

    def display_chat_history():
        for message in st.session_state.chat_history:
            st.write(f"{message['sender']}: {message['content']}")

    user_question = st.chat_input("Ask a Question from your File")
    if user_question:
        st.session_state.chat_history.append({'sender': "You", 'content': user_question})
        
        # Display the updated chat history with the user's question
        display_chat_history()
        
        response_placeholder = st.empty()
        response = ""
        # Simulate streaming response by splitting it into chunks
        answer = user_input(user_question, loc)
        for char in answer:
            response += char
            response_placeholder.write(f"Bot: {response}") 
            time.sleep(0.01)  # Adjust this sleep to control streaming speed

        # Append bot's response to chat history after complete answer is streamed
        st.session_state.chat_history.append({'sender': "Bot", 'content': response})

def user_input(query, loc):
    vectordb1 = Chroma(persist_directory=loc, embedding_function=embeddings)
    docs = vectordb1.similarity_search(query)
    chain = get_conversational_chain(loc)
    result = chain.invoke({"question": query})
    return result["answer"]

if __name__ == "__main__":
    main()
